## Movies_analysis using Visual Descriptors 

This repository implements the methods described in the paper **Automatic Film Classification Based on Low level_PLOS**.  

Additionally, this repository contains the dataset employed in this paper as well as some utilities to extend the size of this database and comparison with the proposed method. 

The repository is organized as follows:

* **Dataset:** contains to subfolders with the data extraction for full movies and trailers.  It contains the datasets for the Visual Descriptors, audio features and Visual features.
* **Utilities:** contains some tools to download the trailers / movies, parse the data and extract the features, (please, use only for research purposes).


# Getting Started


## Install On your local PC
#### Download Sources

use git


```bash
git clone https://gitlab.com/tavitto16/Movies_analysis.git
```
The utilities folder contains two files:

* **clean_data.py:** Some functions to download youtube videos, apply the characterizator as well as to parse the files into a dataFrame. 

* **utilities_EDA.py:** Functions to parse the dataFrame and extract some useful graphs ( histogram, correlation ).   

Moreover, we provide a folder containing the Datasets: 

* Descriptors.csv
* Visual_Descriptors.csv
* Audio_Descriptors.csv

Every file contains the data parsed for the mentioned features as described in our paper. The dataset employed was the [LMTD (Large Movie Trailers Dataset)](https://github.com/jwehrmann/lmtd/blob/master/README.md) . 

Please check the implementation details in our paper.

To run the algorithm, just type:

```bash
python main.py
```
Please, check the configuration parameters in main.py. Replace for the ones of your preference (PATH, GPU...)


# Download dependencies


# Requirements

This code has been tested using:

* Python 3.6
* Pytorch
* Pandas


# License

[MIT](https://opensource.org/licenses/MIT)


# Acknowledgements

If you wish to employ this source, Please reference 

If you have any questions, do not hesitate in contact Federico Álvarez García (fag@gatv.ssr.upm.es) and Gustavo Hernández (ghp@gatv.ssr.upm.es)

To download the visual descriptors extractor, please contact my labmate Juan Pedro López (jlv@gatv.ssr.upm.es)


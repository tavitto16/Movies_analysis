import utils, torch, time, os, pickle
import numpy as np
import torch.nn as nn
import torch.optim as optim
import pandas as pd
from torch.utils.data.dataset import Dataset


#from dataloader import dataloader

def accuracy(out, labels):    
    a = labels.size()
    outputs = torch.argmax(out, dim=1)
    return float(float(torch.sum(outputs==labels))/float(list(a)[0]))
#
    
def accuracy_classes(out, labels):    
    labels_map = { 'Action' : 0 , 'Adventure': 1 , 'Comedy': 2 ,  'Crime' : 3 , 'Drama' : 4 , 'Horror':  5 , 'Romance': 6 , 'SciFi' : 7  ,  'Thriller' : 8 , 'Unknown' : 9 };
    accuracies=[]
    recall= []
    precisions = []
    fprs = []
    for i in labels_map:        
        a = labels==labels_map[i]  #items of the particular class
        if torch.sum(a).item()> 0:            
            a_fake = labels!=labels_map[i] #items that don not belong to this clase (rest of subset)            
            outputs_class = torch.argmax(out[a][:], dim=1) # predictions on clase i 
            outputs_other = torch.argmax(out[a_fake][:], dim=1) # predictions on rest of classes        
            TP = torch.sum(outputs_class==labels[a]).item()   #TP class i, right
            TN = torch.sum(outputs_other==labels[a_fake]).item()  # TN  I say not i , right
            FP = torch.sum(outputs_class!=labels_map[i]).item() #PF  I  say class i, wrongly
            FN= torch.sum(outputs_other==labels_map[i]).item()  # FN: I say different class, but it was class i
        else:
            TP = 0
            TN = 0
            FP = 0
            FN = 0             
        try:
            TPR = float( float(TP) / float(TP+FN) )  #RECALL        
        except:
            TPR=0
        try:
            PPV = float( float(TP) / float(TP+FP) )  #Precision
        except:
            PPV = 0
        try:
            ACC = float( float(TP+TN) /float(TP+TN+FP+FN))    #Accuracy        
        except:
            ACC = 0
        try:         
            FPR = float( float(FP) / float(TN+FP) )  # False PRN
        except:
            FPR= 1
        accuracies.append(ACC) 
        recall.append(TPR)
        precisions.append(PPV)
        fprs.append(FPR)
    return accuracies,recall,precisions,fprs


class CustomDatasetFromCSV(Dataset):
    def __init__(self, path_to_file, features_type, file_name, transforms=None):
        """
		Args:
		    csv_path (string): path to csv file
		    height (int): image height
		    width (int): image width
		    transform: pytorch transforms for transforms and tensor conversion
		"""
        self.features_type = features_type
        self.path_to_file = path_to_file
        self.file_name = file_name
        if self.features_type == 'All':                   
            #if self.file_name == 'All':
                #self.raw_data = pd.read_csv(self.path_to_file+'/Trailers/',sep='\t', lineterminator='\n') 
            #os.chdir(self.path_to_file)
            self.raw_data1 = pd.read_csv(self.path_to_file+'/Descriptors.csv',sep='\t', lineterminator='\n') 
            self.raw_data1_norm =  (self.raw_data1.iloc[:,7:]-self.raw_data1.iloc[:,7:].mean())/(self.raw_data1.iloc[:,7:].max()-self.raw_data1.iloc[:,7:].min())
            self.raw_data2 = pd.read_csv(self.path_to_file+'/Audio_descriptors.csv',sep='\t', lineterminator='\n') 
            self.raw_data2_norm =  (self.raw_data2.iloc[:,:]-self.raw_data2.iloc[:,:].mean())/(self.raw_data2.iloc[:,:].max()-self.raw_data2.iloc[:,:].min())
            self.raw_data3 = pd.read_csv(self.path_to_file+'/Visual_features.csv',sep='\t', lineterminator='\n') 
            self.raw_data3_norm =  (self.raw_data3.iloc[:,:]-self.raw_data3.iloc[:,:].mean())/(self.raw_data3.iloc[:,:].max()-self.raw_data3.iloc[:,:].min())
            self.raw_data = pd.concat([self.raw_data1_norm, self.raw_data2_norm,self.raw_data3_norm], axis=1)
            #self.raw_data = pd.concat([self.raw_data1, self.raw_data2,self.raw_data3], axis=0, sort=False)
        elif self.features_type == 'Descriptors_Visual' :
            self.raw_data1 = pd.read_csv(self.path_to_file+'/Descriptors.csv',sep='\t', lineterminator='\n') 
            self.raw_data1_norm =  (self.raw_data1.iloc[:,7:]-self.raw_data1.iloc[:,7:].mean())/(self.raw_data1.iloc[:,7:].max()-self.raw_data1.iloc[:,7:].min())
            self.raw_data3 = pd.read_csv(self.path_to_file+'/Visual_features.csv',sep='\t', lineterminator='\n') 
            self.raw_data3_norm =  (self.raw_data3.iloc[:,:]-self.raw_data3.iloc[:,:].mean())/(self.raw_data3.iloc[:,:].max()-self.raw_data3.iloc[:,:].min())
            self.raw_data = pd.concat([self.raw_data1_norm, self.raw_data3_norm], axis=1)            
        else :    #self.features_type == 'Descriptors':
            self.raw_data1 = pd.read_csv(self.path_to_file+'/Descriptors.csv',sep='\t', lineterminator='\n') 
            self.raw_data =  (self.raw_data1.iloc[:,7:]-self.raw_data1.iloc[:,7:].mean())/(self.raw_data1.iloc[:,7:].max()-self.raw_data1.iloc[:,7:].min())   
        #else self.features_type == 'Visual':
        self.labels_map = { 'Action' : 0 , 'Adventure': 1 , 'Comedy': 2 ,  'Crime' : 3 , 'Drama' : 4 , 'Horror':  5 , 'Romance': 6 , 'SciFi' : 7  ,  'Thriller' : 8 , 'Unknown' : 9 };
        self.rows, self.columns = self.raw_data.shape
        #self.x_def = torch.zeros(self.rows,self.columns-7)
        #self.labels = torch.zeros(self.rows,len(self.labels_map)).long()

    def __getitem__(self, index):
        #try:
        #print('eficiente')
        x_def = torch.zeros([1,self.columns])
        labels = torch.zeros([1,len(self.labels_map)]).long()
        #for i in self.raw_data.iterrows():
        genres = self.raw_data1.iloc[index,5].split(',')  # extract only the main genre
        if genres[0].strip() in self.labels_map:
            labels[0][int(self.labels_map[genres[0].strip()])] = 1
        elif len(genres)>=2:
            if genres[1].strip() in self.labels_map:
                labels[0][int(self.labels_map[genres[1].strip()])] = 1
            else:
                labels[0][9] = 1
        else:
            labels[0][9] = 1
        x_def[0][:]= torch.tensor(self.raw_data.iloc[index,:])
        return (x_def,labels)          
    def __len__(self):
        return self.rows
    
##----------------------------------------------------------


class discriminator(nn.Module):    
    def __init__(self, input_dim=28, output_dim=10, medium_layer=100):
        super(discriminator, self).__init__()
        self.input_dim    = input_dim        
        self.output_dim   = output_dim
        self.medium_layer = medium_layer
        
        self.fc = nn.Sequential(
            nn.Linear(self.input_dim, self.medium_layer),
            nn.BatchNorm1d(self.medium_layer),
            nn.LeakyReLU(0.2),
            nn.Linear(self.medium_layer, self.output_dim),
            nn.Sigmoid(),
        )
        utils.initialize_weights(self)

    def forward(self, input):
        #x = input
        x = input.view(-1,self.input_dim)
        x = self.fc(x)        
        return x

class GMA_All(object):
    def __init__(self, args):
        # parameters
        self.epoch 		= args.epoch
        self.sample_num = 100
        self.batch_size = args.batch_size
        self.save_dir 	= args.save_dir
        self.result_dir = args.result_dir
        self.Datasets 	= args.Datasets
        self.log_dir 	= args.log_dir
        self.gpu_mode 	= args.gpu_mode
        self.model_name = args.model_name
        self.medium_layer = args.medium_layer
        self.z_dim 		  = 62
        self.Audio        = args.Audio
        self.Image 		  = args.Image
        self.Descriptor_name = args.Descriptor_name
        self.class_num = args.class_num
        if args.Descriptor == True and args.Audio == True and args.Image == True:
        	self.Descriptor = 'All' 
        elif args.Descriptor == True and args.Image == True:
            self.Descriptor = 'Descriptors_Visual'
        else:
            self.Descriptor = "Only_Desc" 
 
        # load dataset
        #self.data_loader = dataloader(self.dataset,  self.batch_size, )
        #funciona
        #self.data_loader = dataloader(self.Datasets, self.batch_size, self.Descriptor)
        self.data_loader1 =CustomDatasetFromCSV(self.Datasets,  self.Descriptor,  self.Descriptor_name)

        self.data_loader = torch.utils.data.DataLoader(dataset=self.data_loader1, batch_size= self.batch_size,     shuffle=False)
        #print(self.data_loader.__dict__)
        data = self.data_loader.__iter__().__next__()[0]
        #data=CustomDatasetFromCSV(self.Datasets,  self.Descriptor,  self.Descriptor_name)
        # networks init
        #N_samples,N_feat = self.data_loader[0].size()
        #print(self.data_loader[0].size())
        self.D = discriminator(input_dim=data.shape[2], output_dim=self.class_num, medium_layer=self.medium_layer)
        self.D_optimizer = optim.Adam(self.D.parameters(), lr=args.lrD, betas=(args.beta1, args.beta2))

        if self.gpu_mode:
            self.D.cuda()
            self.CE_loss = nn.CrossEntropyLoss().cuda()
            self.l1_crit = nn.L1Loss(size_average=False).cuda()

            #self.BCE_loss = nn.BCELoss().cuda()
        else:
            self.l1_crit = nn.L1Loss(size_average=False)
            self.CE_loss = nn.CrossEntropyLoss()
            #self.BCE_loss = nn.BCELoss()

        print('---------- Networks architecture -------------')
        utils.print_network(self.D)
        print('-----------------------------------------------')

        # fixed noise
        self.sample_z_ = torch.rand((self.batch_size, self.z_dim))
        if self.gpu_mode:
            self.sample_z_ = self.sample_z_.cuda()

    def train(self):
        self.train_hist = {}
        self.train_hist['D_loss'] = []
        self.train_hist['per_epoch_time'] = []
        self.train_hist['total_time'] = []
        self.train_hist['accuracy'] = []
        self.train_hist['accuracy_classes'] =[]
        self.train_hist['recall_classes'] = []
        self.train_hist['precision_classes'] =  []        
        self.train_hist['FPR_classes'] = []        
        self.lambda1 = 0.05
        self.D.train()
        print('training start!!')
        start_time = time.time()
        for epoch in range(self.epoch):
            epoch_start_time = time.time()
            for iter, (x_, y_) in enumerate(self.data_loader):
                if iter == self.data_loader.dataset.__len__() // self.batch_size:
                    break
                #z_ = torch.rand((self.batch_size, self.z_dim))
                #y_vec_ = torch.zeros((self.batch_size, self.class_num)).scatter_(1, y_.type(torch.LongTensor).unsqueeze(1), 1)
                x_ = x_.view(self.batch_size,-1)
                y_ = y_.view(self.batch_size,-1)
                if self.gpu_mode:
                    x_ , y_vec_ = x_.cuda(), y_.cuda()
                else:
                    y_vec_ = y_
                # update D network
                self.D_optimizer.zero_grad()
                C_real = self.D(x_)
                #for param in self.D.parameters():
                #    reg_loss += self.l1_crit(param)                
                #print(torch.max(y_vec_, 1)[1])
                #print(toch.max(C_real,1)[1])    

                all_linear1_params = torch.cat([x.view(-1) for x in self.D.fc.parameters()])
                #l1_regularization =  self.lambda1 * torch.norm(all_linear1_params, 1)
                C_real_loss = self.CE_loss(C_real, torch.max(y_vec_, 1)[1])  # + l1_regularization # + self.factor * reg_loss

                self.train_hist['D_loss'].append(C_real_loss.item())
                C_real_loss.backward()
                self.D_optimizer.step()
                pres = accuracy(C_real,torch.max(y_vec_, 1)[1])
                #accuracies,recall,precisions,fprs
                accuracy_class, recall_class,precision_classes,fprs_classes = accuracy_classes(C_real,torch.max(y_vec_, 1)[1])
                self.train_hist['accuracy'].append(pres)                
                self.train_hist['accuracy_classes'].append(accuracy_class)
                self.train_hist['recall_classes'].append(recall_class)
                self.train_hist['precision_classes'].append(precision_classes)
                self.train_hist['FPR_classes'].append(fprs_classes)
                #precision.append(pres)
                print("Epoch: [%2d] [%4d/%4d] D_loss: %.8f Accuracy: %.8f" %
                      ((epoch + 1), (iter + 1), self.data_loader.dataset.__len__() // self.batch_size, C_real_loss.item(), pres ))
            #precision.append(accuracy(torch.max(torch.round(C_real),1)[1],torch.max(y_vec_[0], 1)[1]))
            self.train_hist['per_epoch_time'].append(time.time() - epoch_start_time)
            #with torch.no_grad():
            #    self.visualize_results((epoch+1))
        self.train_hist['total_time'].append(time.time() - start_time)
        print("Avg one epoch time: %.2f, total %d epochs time: %.2f" % (np.mean(self.train_hist['per_epoch_time']),
              self.epoch, self.train_hist['total_time'][0]))
        print("Training finish!... save training results")
        self.save()
        utils.loss_plot(self.train_hist, os.path.join(self.save_dir, self.Datasets, self.model_name), self.model_name)

    def visualize_results(self, epoch, fix=True):
        self.D.eval()
        if not os.path.exists(self.result_dir + '/' + self.Datasets + '/' + self.model_name):
            os.makedirs(self.result_dir + '/' + self.Datasets + '/' + self.model_name)

        tot_num_samples = min(self.sample_num, self.batch_size)
        #image_frame_dim = int(np.floor(np.sqrt(tot_num_samples)))

        if fix:
            """ fixed noise """
            samples = self.D(self.sample_z_)
        else:
            """ random noise """
            sample_z_ = torch.rand((self.batch_size, self.z_dim))
            if self.gpu_mode:
                sample_z_ = sample_z_.cuda()
            samples = self.D(sample_z_)

        if self.gpu_mode:
            samples = samples.cpu().data.numpy().transpose(0, 2, 3, 1)
        else:
            samples = samples.data.numpy().transpose(0, 2, 3, 1)

        samples = (samples + 1) / 2

    def save(self):
        save_dir = os.path.join(self.save_dir, self.Datasets, self.model_name)
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)
        
        torch.save(self.D.state_dict(), os.path.join(save_dir, self.model_name + '_D.pkl'))

        with open(os.path.join(save_dir, self.model_name + '_history.pkl'), 'wb') as f:
            pickle.dump(self.train_hist, f)

    def load(self):
        save_dir = os.path.join(self.save_dir, self.Datasets, self.model_name)
        self.D.load_state_dict(torch.load(os.path.join(save_dir, self.model_name + '_D.pkl')))
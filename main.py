import argparse, os, torch, time

#import GMA_Audio
#import GMA_Image
#from GMA_Descriptor import GMA_Descriptor
from GMA_All import GMA_All



"""parsing and configuration"""
def parse_args():
    desc = "Pytorch implementation of a Genre-Movie Classification system "
    parser = argparse.ArgumentParser(description=desc)
    #parser.add_argument('--Audio', type=bool, default=False, help='Type of data used for classification')
    parser.add_argument('--Audio', type=bool, default=False, help='Decide wheter use Audio features or not: default yes (True)')
    parser.add_argument('--Image', type=bool, default=True, help='Decide wheter use Image features or not: default yes (True)')
    parser.add_argument('--Descriptor',   type=bool, default=True, help='Decide wheter use Descriptors features or not: default yes (True)')
    #parser.add_argument('--All_features', type=bool,  default=True, help='Decide wheter use Descriptors features or not: default yes (True)')
    parser.add_argument('--Descriptor_name', type=str, default='Descriptors.csv', help='Choose the file name')
    #parser.add_argument('--dataset', type=str, default='cifar10', choices=['mnist', 'fashion-mnist', 'cifar10', 'cifar100', 'svhn', 'stl10', 'lsun-bed'],
    #                    help='The name of dataset')
    #parser.add_argument('--split', type=str, default='', help='The split flag for svhn and stl10')
    parser.add_argument('--class_num', type=int, default=10, help='Number of Genres to classify')
    parser.add_argument('--epoch', type=int, default=50, help='The number of epochs to run')    
    parser.add_argument('--batch_size', type=int, default=64, help='The size of batch')
    parser.add_argument('--medium_layer', type=int, default=100, help='The size of medium layer')
    parser.add_argument('--save_dir', type=str, default='models', help='Directory name to save the model')
    parser.add_argument('--result_dir', type=str, default='results', help='Directory name to save the generated images')
    parser.add_argument('--log_dir', type=str, default='logs', help='Directory name to save training logs')
    parser.add_argument('--Datasets', type=str, default='Datasets', help=' Directory to read the Datasets ')
    parser.add_argument('--model_name', type=str, default='model_name', help=' Directory to save the Modelname ')
    parser.add_argument('--lrG', type=float, default=0.0002)
    parser.add_argument('--lrD', type=float, default=0.0002)
    parser.add_argument('--beta1', type=float, default=0.555)
    parser.add_argument('--beta2', type=float, default=0.999)
    parser.add_argument('--gpu_mode', type=bool, default=True)
    parser.add_argument('--benchmark_mode', type=bool, default=True)
    return check_args(parser.parse_args())

"""checking arguments"""
def check_args(args):
    # --save_dir
    abs_root='/media/administrador/1B34372465159C8C/lmtd/Movies_analysis/'
    args.save_dir= abs_root+args.save_dir
    if not os.path.exists(args.save_dir):
        os.makedirs(args.save_dir)

    #--Datsets dir
    args.Datasets= abs_root+args.Datasets    
    if not os.path.exists(args.Datasets):
        os.makedirs(args.Datasets)

    # --result_dir
    args.result_dir = abs_root+args.result_dir
    if not os.path.exists(args.result_dir):
        os.makedirs(args.result_dir)

    # --log_dir
    args.log_dir = abs_root+args.log_dir
    if not os.path.exists(args.log_dir):
        os.makedirs(args.log_dir)

    # --epoch
    try:
        assert args.epoch >= 1
    except:
        print('number of epochs must be larger than or equal to one')

    # --batch_size
    try:
        assert args.batch_size >= 1
    except:
        print('batch size must be larger than or equal to one')

    return args

#%%%%


"""main"""
def main():
    # parse arguments
    args = parse_args()
    if args is None:
        exit()

    if args.benchmark_mode:
        torch.backends.cudnn.benchmark = True
        # declare instance for process the data
#    # 
#    check = False
#    if args.Audio == True and args.Image == False and args.Descriptor == False:
#        GMA1 = GMA_Audio(args)
#        check = True
#    if args.Image == True and args.Audio == True and args.Descriptor == False:
#        GMA2 = GMA_Image(args)   
#        check = True
#    if args.Descriptor == True and args.Audio == False and args.Image == False:
#        GMA3 = GMA_Descriptor(args)
#        check = True
    if args.Image == True or args.Descriptor == True or args.Audio == True :
        GMA_all = GMA_All(args)        
    else :
        raise Exception("There is no option for data of Audio or Image or Descriptors")

        # launch the graph in a session
    t0 = time.time()
    GMA_all.train()
    t1 = time.time()
    print(" [*] Training finished!")

    # visualize learned generator
    #GMA_all.visualize_results(args.epoch)
    print(" [*] Testing finished!")
    print("The process started at:")
    print(t0)
    print("The process ended at:")
    print(t1)
    print(" The process has taken")
    print(t1-t0)


if __name__ == '__main__':
    main()
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 26 16:17:14 2018

@author: gustavo
"""

import sys
#sys.path.append('../')


import os
from lmtd9 import LMTD
from lmtd9 import database as db
import numpy as np
import pandas as pd
import csv
import re

#%%

lmtd = LMTD() # Creating an LMTD object for handling lmtd data

# First instance of training data
trailer_id = lmtd.train_ids[9]
trailer_id = lmtd.train_ids
print type(trailer_id)

# Returns a dictionary in which keys are the queried trailer_ids
movie_data = lmtd.get_data_by_trailer_ids(trailer_id)

# Lets see some data from the selected trailer_id
print '--' * 15
print 'Data example'
print '--' * 15

print "todo "  , movie_data.viewitems()
print 'trailer id: ', trailer_id
print 'movie id  : ', movie_data[trailer_id]['imdbID']
print 'genres db : ', movie_data[trailer_id]['Genre']
print  'MOVIES TITLE : ' , movie_data[trailer_id]['Title']
# Checking genres 
print '--' * 15
print 'Checking Genres'
print '--' * 15
print 'labels       : ', lmtd.train_labels[0]
print 're-converted : ', lmtd.binary_label_to_genre(lmtd.train_labels[0])[0]

#%%%%%

#df_moviedata = pd.DataFrame(movie_data,columns=movie_data.viewitems())

pelis_con_ano = 'pelis_paper_completas.xlsx'
#variable = open(pelis_paper, 'r').read()
pelis_con_ano = pd.read_excel(io=pelis_con_ano, sheet_name='Table 1')
#pelis_lmtd = pd.read_csv(csvfile)

#%%%%

anos  = []
for i in range(0,len(pelis_paper['Title'])):
    division = pelis_con_ano['Title'][i].split('(')    
    anos.append(division[len(division)-1][0:-1])
        




#%%%%##################33   EXAMPLE DUMMYYYY  RECOVER MOVIES ########################################3
#Recover id some MOVIES

# pelis = [1345,2323,524,27,933,2755,2087,2526,2094,401,2180,2787,69,1253,1075,1636,482,1396,1574,994,1624
# ,770
# ,181
# ,2803
# ,1166
# ,786
# ,2162
# ,2842
# ,371
# ]

# pelis_list = pelis_lmtd['id'].iloc[pelis]

# #lista_pelis = pelis_lmtd.loc[pelis_lmtd['index']==pelis,'id']


#%%%%%%%%%%%######################################################
#removing know words

def limpiar_dataset(pelis_to_clean,clave):
    stoplist = set('for a of the and to in'.split())
    pelis_paper_clean = [[word for word in str(document).lower().split() if word not in stoplist]
         for document in pelis_to_clean[clave]]
    return pelis_paper_clean

#pelis_lmtd_clear = [[word for word in str(document).lower().split() if word not in stoplist]
#     for document in pelis_lmtd['Title']]



#for  j  in range(0,len(pelis_paper['Title'])):
    

[i for i,x in enumerate(pelis_lmtd_clear) if x == pelis_paper_clear[46]]
#%%
a= 7




#%%###########################################################################################

def remove_common_words(sentence):
    print('entra')
    words = sentence.lower().split()
    #removing know words
    #    stoplist = set('for a of the and to in'.split())
    #    texts = [[word for word in str(document).lower().split() if word not in stoplist]
    #         for document in pelis_paper['Title']
    return words

#%%
    

for  i in range(0,len(pelis_paper['Title'])):
    #words= remove_common_words( str(pelis_paper_clear[i]))
    words  = pelis_paper_clear[i]
    #print(words)
    for j in words:
        #print j
        if j in pelis_lmtd_clear:
            print "algo" 
    
    



#%%%%%%%%%%%333333333333333333333333333333333333333333333333333333333333333333333333333333

#Optional... remove years

for i in range(0,len(pelis_paper['Title'])):
    pelis_paper['Title'][i]=re.sub(r'\([^)]*\)', '', pelis_paper['Title'][i])
    pelis_paper['Title'][i]=re.sub(r',', '', pelis_paper['Title'][i])
#pelis_paper['pelicula'][0:]=re.sub(r'\([^)]*\)', '', pelis_paper['pelicula'][0:])





#%%%%
csvfile='lmtd_peli_list.csv'
with open(csvfile, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    writer.writerow(['id','Title','Year','Genre'])        
    for i in movie_data.items():
        writer.writerow([i[0],i[1]['Title'],i[1]['Year'],i[1]['Genre']])        
#        try:
#        #print i
#
#        #movie_data = lmtd.get_data_by_trailer_ids(np.string_(i))
#        #print movie_data[np.string_(i)]['Title']
#        except:
#            print " la peli  con id  no se encuentra "   
#        for val in res:
#            writer.writerow([val]) 

    


#%%%%%################################################################################


LMTD_PATH ='/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd' # insert your $LMTD_PATH here

features_path = os.path.join(LMTD_PATH, 'features', 'lmtd9_resnet152.pickle')
lmtd.load_precomp_features(features_file=features_path)



#%%%

x_valid, x_valid_len, y_valid, valid_ids = lmtd.get_split('valid')



#%%%

#Pruebas recuperadas

import subprocess

from subprocess import call
from subprocess import Popen, PIPE

import sh
process  = subprocess.call(["/.CaracterizadorMovies", video_def])


#%%&%%

#%%%&##3#Pruebas recuperadas   ----  TEST TO RECOVER A YOUTUBE TRAILER 


from pytube import YouTube
yt = YouTube("https://www.youtube.com/watch?v=n06H7OcPd-g")
yt = yt.get('mp4', '720p')
from pytube import YouTube
import os
def downloadYouTube(videourl, path):
    
    yt = YouTube(videourl)
    yt = yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first()
    if not os.path.exists(path):
        os.makedirs(path)
    yt.download(path)



#downloadYouTube('https://www.youtube.com/watch?v=zNyYDHCg06c', '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/')
#%%

from pytube import YouTube
import os

def downloadYouTube(videourl, path):
    
    yt = YouTube(videourl)
    yt = yt.streams.filter(progressive=True, file_extension='mp4').order_by('resolution').desc().first()
    if not os.path.exists(path):
        os.makedirs(path)
    yt.download(path)


downloadYouTube('https://www.youtube.com/watch?v=zNyYDHCg06c', '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/prueba1.mp4')
pelis_paper = 'lista_definitiva.xlsx'
#variable = open(pelis_paper, 'r').read()
pelis_paper = pd.read_excel(io=pelis_paper, sheet_name='Sheet1')
#pelis_lmtd = pd.read_csv(csvfile)
"""
Created on Sun Aug  5 23:07:46 2018

@author: gustavo
"""

#%%#############################################################################################

#REVOVER SOME MOVIES FROM  EXCEL
import subprocess

from subprocess import call
from subprocess import Popen, PIPE
import pandas as pd

import sh
pelis_paper = 'lista_definitiva.xlsx'
#variable = open(pelis_paper, 'r').read()
pelis_paper = pd.read_excel(io=pelis_paper, sheet_name='Sheet1')
#pelis_lmtd = pd.read_csv(csvfile)
pelis_paper
pelis_paper['LMTD'][0]
pelis_paper['LMTD'][1]
for i in range(0,len(pelis_paper['NAME'])):
    if pelis_paper['LMTD'][i] == nan: 
        print "entramos en todo"


#%%%%#####################################################33   PARSEO DE DATOS

import math
import pandas as pd


pelis_paper = 'lista_definitiva.xlsx'
#variable = open(pelis_paper, 'r').read()
pelis_paper = pd.read_excel(io=pelis_paper, sheet_name='Sheet1')
#pelis_lmtd = pd.read_csv(csvfile)

#%%%%%

for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
        print "entramos en todo"
import math


for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
        print "entramos en todo"
        pelis_paper['Youtube_Links'][i]
import math


for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
        print "entramos en todo"
        print pelis_paper['Youtube_Links'][i]
import math
ejecutable = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/build-CaracterizadorMovies-Desktop-Debug/./CaracterizadorMovies'


for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
        path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' + str(pelis_paper['ID_FAUS'][i])
        #print "entramos en todo"
        #print pelis_paper['Youtube_Links'][i]
        downloadYouTube(pelis_paper['Youtube_Links'][i] , path_to_video )
        process  = subprocess.call([ejecutable, path_to_video+'/*'])

#%%#######################################################################################33333



#os.rename(os.listdir(os.curdir)[0],'105.mp4')
#ls
ejecutable = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/build-CaracterizadorMovies-Desktop-Debug/./CaracterizadorMovies'

for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
       path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' + str(pelis_paper['ID_FAUS'][i])
       os.chdir(path_to_video)
       new_name = path_to_video+'/'+str(pelis_paper['ID_FAUS'][i])+'.mp4'
       os.rename(os.listdir(os.curdir)[0],new_name)
       #process  = subprocess.call([ejecutable, path_to_video+'/*'])

#%%%

ejecutable = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/build-CaracterizadorMovies-Desktop-Debug/./CaracterizadorMovies'

for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
       path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' + str(pelis_paper['ID_FAUS'][i])
       os.chdir(path_to_video)
       new_name = path_to_video+'/'+str(pelis_paper['ID_FAUS'][i])+'.mp4'
       os.rename(os.listdir(os.curdir)[0],new_name)
       #process  = subprocess.call([ejecutable, path_to_video+'/*'])

#%%
       
import math

for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
        #print "entramos en todo"
        #print pelis_paper['Youtube_Links'][i]
        path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' + str(pelis_paper['ID_FAUS'][i])
        downloadYouTube(pelis_paper['Youtube_Links'][i] , path_to_video )
pelis_paper = 'lista_definitiva.xlsx'
#variable = open(pelis_paper, 'r').read()
pelis_paper = pd.read_excel(io=pelis_paper, sheet_name='Sheet1')
#pelis_lmtd = pd.read_csv(csvfile)
pelis_paper = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/lista_definitiva.xlsx'
#variable = open(pelis_paper, 'r').read()
pelis_paper = pd.read_excel(io=pelis_paper, sheet_name='Sheet1')
#pelis_lmtd = pd.read_csv(csvfile)

#%%%

import math


for i in range(177,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
        #print "entramos en todo"
        #print pelis_paper['Youtube_Links'][i]
        path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' + str(pelis_paper['ID_FAUS'][i])
        downloadYouTube(pelis_paper['Youtube_Links'][i] , path_to_video )
        
#%%5%%%
        
        
ejecutable = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/build-CaracterizadorMovies-Desktop-Debug/./CaracterizadorMovies'

for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
       path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' + str(pelis_paper['ID_FAUS'][i])
       os.chdir(path_to_video)
       new_name = path_to_video+'/'+str(pelis_paper['ID_FAUS'][i])+'.mp4'
       os.rename(os.listdir(os.curdir)[0],new_name)
       #process  = subprocess.call([ejecutable, path_to_video+'/*'])
     
#%%%
        
ejecutable = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/build-CaracterizadorMovies-Desktop-Debug/./CaracterizadorMovies'
for i in range(0,len(pelis_paper['NAME'])):
    if math.isnan(pelis_paper['LMTD'][i]):
       path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' + str(pelis_paper['ID_FAUS'][i])
       os.chdir(path_to_video)
       process  = subprocess.call([ejecutable, os.listdir(os.curdir)[0] ])

#%%
#Quitar filas con nan
df2 = pelis_paper.copy()
df2.dropna(subset=['LMTD'],inplace=True)
       
       
#%%
import glob
len_videos = 10


for i in df2.iterrows():
    print i[0]
    print str(int(pelis_paper['LMTD'][i[0]]))
    if str(int(pelis_paper['LMTD'][i[0]])) == '2467':
        continue
    
    path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' 
    os.chdir(path_to_video)
    new_name = str(int(pelis_paper['ID_FAUS'][i[0]]))
    try:
        if os.path.exists(str(int(pelis_paper['LMTD'][i[0]]))):
            os.rename(str(int(pelis_paper['LMTD'][i[0]])),new_name)        
        else:
            print "la cagamos"
    except:
        print "revisar" + str(int(pelis_paper['LMTD'][i[0]]))
    os.chdir(path_to_video+new_name)
    #+ str(int(pelis_paper['LMTD'][i[0]]))
    ceros = '0000000000'
    value_video = str(int(pelis_paper['ID_FAUS'][i[0]]))+'.mp4'           
    video_def  =ceros[0:len_videos-len(value_video)]+value_video
    os.rename(glob.glob('*.mp4')[0],video_def)
    os.rename(glob.glob('*.xml')[0],video_def+'.xml')
    os.rename(glob.glob('*_descriptor.txt')[0],video_def+'_descriptor.txt')
    os.rename(glob.glob('*_cortes.txt')[0],video_def+'_cortes.txt')
    os.rename(glob.glob('*_escenas.txt')[0],video_def+'_escenas.txt')   
    
#%%

# create and instance of the IMDb class
from imdb import IMDb
ia = IMDb()

# get a movie and print its director(s)
the_matrix = ia.get_movie('0133093')

print(the_matrix['director'])
print(the_matrix['title'])
print(the_matrix['year'])
print(the_matrix['genre'])


# show all the information sets avaiable for Movie objects
print(ia.get_movie_infoset())

# update a Movie object with more information
ia.update(the_matrix, ['technical'])
# show which keys were added by the information set
print(the_matrix.infoset2keys['technical'])
# print one of the new keys
print(the_matrix.get('cinematographic process'))

# search for a person
for person in ia.search_person('Mel Gibson'):
    print(person.personID, person['name'])

# get the first result of a company search,
# update it to get the basic information
ladd_company = ia.search_company('The Ladd Company')[0]
ia.update(ladd_company)
# show the available information and print some
print(ladd_company.keys())
print(ladd_company.get('production companies'))

# get 5 movies tagged with a keyword
dystopia = ia.get_keyword('dystopia', results=5)

# get a Character object
deckard = ia.search_character('Rick Deckard')[0]
ia.update(deckard)
print(deckard['full-size headshot'])

# get top250 and bottom100 movies
top250 = ia.get_top250_movies()
bottom100 = ia.get_bottom100_movies()


#%%%%
#Cargar Pelis IMDB

imdbfile = 'data.tsv'
#variable = open(pelis_paper, 'r').read()
#pelis_paper = pd.read_excel(io=pelis_paper, sheet_name='Sheet1')
pelis_imdb = pd.read_csv(imdbfile, sep='\t', lineterminator='\n')
#DataFrame.from_csv('c:/~/trainSetRel3.txt', sep='\t', header=0)


#%%% ###############################################################
#pelis_imdb.loc[(pelis_imdb['primaryTitle'].str.lower() == pelis_paper_clear[0][0] ) & (pelis_imdb['titleType'].str.lower() == 'movie'  ),]
#pelis_paper_clear = limpiar_dataset(pelis_paper,'Title')
pelis_paper_clear = limpiar_dataset(pelis_paper,'Title')
#%%
ids_imdb = []
genres = []

for i in range(0,len(pelis_paper['Title'])):
    try:
        print i,len(pelis_paper_clear[i]),pelis_paper_clear[i][0]
        
        if len(pelis_paper_clear[i]) >1:
            print("entra")
            id_peli = pelis_imdb.loc[(pelis_imdb['primaryTitle'].str.contains(str(pelis_paper_clear[i][0]),case=False)==True ) &  (pelis_imdb['primaryTitle'].str.contains(str(pelis_paper_clear[i][1]),case=False)==True ) & (pelis_imdb['titleType'].str.match('movie') == True) & ( pelis_imdb['startYear'].str.match(str(anos[i])) ==True) ,]
        else:
            print("sale")
            #pelis_imdb['primaryTitle'].str.lower()
            print str(pelis_paper['Title'][i])
            #id_peli = pelis_imdb.loc[(pelis_imdb['primaryTitle'].str.contains(str(pelis_paper['Title'][i])) ) & (pelis_imdb['titleType'].str.lower() == 'movie'  ) & (pelis_imdb['startYear']==anos[i]),]
            id_peli = pelis_imdb.loc[(pelis_imdb['primaryTitle'].str.contains(str(pelis_paper_clear[i][0]),case=False)==True ) & (pelis_imdb['titleType'].str.match('movie') == True) & ( pelis_imdb['startYear'].str.match(str(anos[i])) ==True) ,]
            #id_peli = pelis_imdb.loc[(pelis_imdb['primaryTitle'].str.contains(str(pelis_paper['Title'][i]))==True ) & (pelis_imdb['titleType'].str.contains('movie') == True  ) & (pelis_imdb['startYear'].str.contains(anos[i])==True),]
            #print(id_peli)
        ids_imdb.append(id_peli['tconst'].values[0])    
        genres.append(id_peli['genres'].values[0])
    except:
        ids_imdb.append(0)    
        genres.append(0)        
        print "revisar" + str(i)
        
    




#pelis_imdb.loc[(pelis_imdb['primaryTitle'].str.lower() == pelis_paper_clear[0][0] ) & (pelis_imdb['titleType'].str.lower() == 'movie'  ) & (pelis_imdb['startYear']==anos[0]),]

#%%
        
#%%
import glob
import xml.etree.ElementTree

BWR = []

len_videos = 10

path_to_video = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/trailers/' 
for i in pelis_paper.iterrows():
    print i[0]
    print str(int(pelis_paper['ID_FAUS'][i[0]]))
    #os.chdir(path_to_video)
    new_name = str(int(pelis_paper['ID_FAUS'][i[0]]))
    os.chdir(path_to_video+new_name)
    ceros = '0000000000'
    value_video = str(int(pelis_paper['ID_FAUS'][i[0]]))+'.mp4'
    if os.path.isfile(value_video):            
        video_def = value_video
    else:
        video_def  = ceros[0:len_videos-len(value_video)]+value_video    
    print video_def+'.xml'
    e = xml.etree.ElementTree.parse(video_def+'.xml').getroot()
    for value in e:
        print("entra")
        BWR.append(value.text)
            
    
#    ceros = '0000000000'
#    value_video = str(int(pelis_paper['ID_FAUS'][i[0]]))+'.mp4'           
#    video_def  =ceros[0:len_videos-len(value_video)]+value_video
#    os.rename(glob.glob('*.mp4')[0],video_def)
#    os.rename(glob.glob('*.xml')[0],video_def+'.xml')
#    os.rename(glob.glob('*_descriptor.txt')[0],video_def+'_descriptor.txt')
#    os.rename(glob.glob('*_cortes.txt')[0],video_def+'_cortes.txt')
#    os.rename(glob.glob('*_escenas.txt')[0],video_def+'_escenas.txt')   

#%%%
        


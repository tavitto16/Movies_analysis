#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 13:54:41 2018

@author: gustavo
"""

#EDA para peliculas

import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import warnings
import hdbscan
import seaborn as sns


from collections import Counter
from bokeh.io import output_notebook, show
from bokeh.models import Range1d
from scipy.stats import gaussian_kde, anderson, skew, kurtosis, gamma, entropy
from itertools import  count, cycle
from helpers import block_heatmap, scatter_with_hover, plot_histogram, counter_histogram, bar_color
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.decomposition import FastICA, PCA
from sklearn.manifold import MDS, TSNE
#from statsmodels.distributions.empirical_distribution import ECDF
#from ipywidgets import interact
from matplotlib.colors import ListedColormap

#%matplotlib inline
#warnings.filterwarnings('ignore')
#plt.rcParams['figure.figsize'] = (12, 5)
#output_notebook()

#%%%     CARGAMOS EL ARCHIVO, REEMPLAZAMOS LOs NAN POR LA MEDIA

#df = pd.read_excel("Data_Cortex_Nuclear.xls", index_col=0)

pelis_paper = '/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/lista_definitiva_def.xlsx'
#variable = open(pelis_paper, 'r').read()
df = pd.read_excel(io=pelis_paper, sheet_name='Sheet1')

df_copy = df.fillna(df.mean())
df_numeric = df.filter(like="_N", axis=1)

#%%     GENEERAL STATISTICS OF DATA

description = df.describe()
coef_variation = description.loc["std"] / description.loc["mean"]
description.loc["cova"] = coef_variation
description.sort_values(by="cova", axis=1)

description1 = df_copy.describe()
coef_variation1 = description1.loc["std"] / description1.loc["mean"]
description1.loc["cova"] = coef_variation1
description1.sort_values(by="cova", axis=1)


#%%     VIDEO 2 IMAGE CONVERSION

import cv2
vidcap = cv2.VideoCapture('/media/gustavo/5D7F5FF157492AC1/PELICULAS/lmtd/Finding Nemo- Dorys Best Moments.mp4')
success,image = vidcap.read()
count = 0
while success:
  cv2.imwrite("frame%d.jpg" % count, image)     # save frame as JPEG file      
  success,image = vidcap.read()
  print('Read a new frame: ', success)
  count += 1
  
#%%     NORMALIZATION OF DATA
# minmax_normalized_df = pd.DataFrame(MinMaxScaler().fit_transform(df_numeric),
#                                    columns=df_numeric.columns, index=df_numeric.index)

#standardized_df = pd.DataFrame(StandardScaler().fit_transform(df_numeric), columns=df_numeric.columns,
#                               index=df_numeric.index)



ecdf_normalized_df = df_numeric.apply(lambda c: pd.Series(ECDF(c)(c), index=c.index))

  
#%%%   GRAFICAMOS. BASADO EN LA GRAF, 1.5 PARECE SER UN BUREN VALOR

plot_histogram(description1.loc["cova"]);

#%%%

high_cova = description1.loc["cova"].where(lambda x: x > 1.5).dropna().sort_values(ascending=False)
high_cova


#%%% HSTOGRAMA DE UBNA VARIABLE

print(df['LUM'].describe())
plt.figure(figsize=(9, 8))
sns.distplot(df['LUM'], color='g', bins=100, hist_kws={'alpha': 0.4});

#%%%%%%%% PREPARAMOS LOS VALORES

list(set(df.dtypes.tolist()))
df_num = df.select_dtypes(include = ['float64', 'int64'])
df_num.head()

#%%%%%   SACAMOS LOS HISTOGRAMAS DE TODOs
df_num.hist(figsize=(16, 20), bins=50, xlabelsize=8, ylabelsize=8); 
# ; avoid having the matplotlib verbose informations

#%%%%    SACAMOS LOS VALORES M;AS CORRELADOS CON UNA VARIABLE (IMI)

df_num_corr = df_num.corr()['IMI'][:-1] # -1 because the latest row is SalePrice
golden_features_list = df_num_corr[abs(df_num_corr) > 0.5].sort_values(ascending=False)
print("There is {} strongly correlated values with imi :\n{}".format(len(golden_features_list), golden_features_list))


#%%   SACAMOS LAS GRAFICAS DE LA RELACION DE UNA VARIABLE CON eL RESTO

for i in range(0, len(df_num.columns), 5):
    sns.pairplot(data=df_num,
                x_vars=df_num.columns[i:i+5],
                y_vars=['IMI'])
    
#%%%%   TODAS LAS CORRELACIONES
    
import operator

individual_features_df = []
for i in range(0, len(df_num.columns) - 1): # -1 because the last column is SalePrice
    tmpDf = df_num[[df_num.columns[i], 'IMI']]
    tmpDf = tmpDf[tmpDf[df_num.columns[i]] != 0]
    individual_features_df.append(tmpDf)
all_correlations = {feature.columns[0]: feature.corr()['IMI'][0] for feature in individual_features_df}
all_correlations = sorted(all_correlations.items(), key=operator.itemgetter(1))
for (key, value) in all_correlations:
    print("{:>15}: {:>15}".format(key, value))
    
#%%

corr = df_num.corr() 
plt.figure(figsize=(12, 10))

sns.heatmap(corr[(corr >= 0.5) | (corr <= -0.4)], 
            cmap='viridis', vmax=1.0, vmin=-1.0, linewidths=0.1,
            annot=True, annot_kws={"size": 8}, square=True);
                                 
#%%

                 
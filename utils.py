import os, gzip, torch
import torch.nn as nn
import numpy as np
import scipy.misc
import imageio
import matplotlib.pyplot as plt
from torchvision import datasets, transforms

def load_mnist(dataset):
    data_dir = os.path.join("./data", dataset)

    def extract_data(filename, num_data, head_size, data_size):
        with gzip.open(filename) as bytestream:
            bytestream.read(head_size)
            buf = bytestream.read(data_size * num_data)
            data = np.frombuffer(buf, dtype=np.uint8).astype(np.float)
        return data

    data = extract_data(data_dir + '/train-images-idx3-ubyte.gz', 60000, 16, 28 * 28)
    trX = data.reshape((60000, 28, 28, 1))

    data = extract_data(data_dir + '/train-labels-idx1-ubyte.gz', 60000, 8, 1)
    trY = data.reshape((60000))

    data = extract_data(data_dir + '/t10k-images-idx3-ubyte.gz', 10000, 16, 28 * 28)
    teX = data.reshape((10000, 28, 28, 1))

    data = extract_data(data_dir + '/t10k-labels-idx1-ubyte.gz', 10000, 8, 1)
    teY = data.reshape((10000))

    trY = np.asarray(trY).astype(np.int)
    teY = np.asarray(teY)

    X = np.concatenate((trX, teX), axis=0)
    y = np.concatenate((trY, teY), axis=0).astype(np.int)

    seed = 547
    np.random.seed(seed)
    np.random.shuffle(X)
    np.random.seed(seed)
    np.random.shuffle(y)

    y_vec = np.zeros((len(y), 10), dtype=np.float)
    for i, label in enumerate(y):
        y_vec[i, y[i]] = 1

    X = X.transpose(0, 3, 1, 2) / 255.
    # y_vec = y_vec.transpose(0, 3, 1, 2)

    X = torch.from_numpy(X).type(torch.FloatTensor)
    y_vec = torch.from_numpy(y_vec).type(torch.FloatTensor)
    return X, y_vec

def load_celebA(dir, transform, batch_size, shuffle):
    # transform = transforms.Compose([
    #     transforms.CenterCrop(160),
    #     transform.Scale(64)
    #     transforms.ToTensor(),
    #     transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
    # ])

    # data_dir = 'data/celebA'  # this path depends on your computer
    dset = datasets.ImageFolder(dir, transform)
    data_loader = torch.utils.data.DataLoader(dset, batch_size, shuffle)

    return data_loader


def print_network(net):
    num_params = 0
    for param in net.parameters():
        num_params += param.numel()
    print(net)
    print('Total number of parameters: %d' % num_params)

def save_images(images, size, image_path):
    return imsave(images, size, image_path)

def imsave(images, size, path):
    image = np.squeeze(merge(images, size))
    return scipy.misc.imsave(path, image)

def merge(images, size):
    h, w = images.shape[1], images.shape[2]
    if (images.shape[3] in (3,4)):
        c = images.shape[3]
        img = np.zeros((h * size[0], w * size[1], c))
        for idx, image in enumerate(images):
            i = idx % size[1]
            j = idx // size[1]
            img[j * h:j * h + h, i * w:i * w + w, :] = image
        return img
    elif images.shape[3]==1:
        img = np.zeros((h * size[0], w * size[1]))
        for idx, image in enumerate(images):
            i = idx % size[1]
            j = idx // size[1]
            img[j * h:j * h + h, i * w:i * w + w] = image[:,:,0]
        return img
    else:
        raise ValueError('in merge(images,size) images parameter ''must have dimensions: HxW or HxWx3 or HxWx4')

def generate_animation(path, num):
    images = []
    for e in range(num):
        img_name = path + '_epoch%03d' % (e+1) + '.png'
        images.append(imageio.imread(img_name))
    imageio.mimsave(path + '_generate_animation.gif', images, fps=5)

def loss_plot(hist, path = 'Train_hist.png', model_name = ''):
    x = range(len(hist['D_loss']))

    y1 = hist['D_loss']
    #y2 = hist['G_loss']

    plt.plot(x, y1, label='D_loss')
    #plt.plot(x, y2, label='G_loss')

    plt.xlabel('Iteration')
    plt.ylabel('Classifier Loss')

    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()

    path = os.path.join(path, model_name + '_loss.png')

    plt.savefig(path)

    plt.close()
    
    y2 = hist['accuracy']
    plt.plot(x, y2, label='Accuracy')
    plt.xlabel('Iteration')
    plt.ylabel('Accuracy')
    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()
    #path = os.path.join(path, model_name + '_accuracy.png')
    plt.savefig(path+'accuracy.png')
    plt.close()

    y_accu0 =[]
    y_accu1=[]  
    y_accu2 = []
    y_accu3 = []
    y_accu4 =[]
    y_accu5 = []
    y_accu6 = []
    y_accu7 =[]
    y_accu8 =[]
    y_rec0 = []
    y_rec1 = []
    y_rec2 = []
    y_rec3 = []
    y_rec4 = []
    y_rec5 = []
    y_rec6 = []
    y_rec7 = []
    y_rec8 =[]
    y_pres0 = []
    y_pres1 = []
    y_pres2 = []
    y_pres3 = []
    y_pres4 = []
    y_pres5 = []
    y_pres6 = []
    y_pres7 =[]
    y_pres8 =[]
    y_prec0 = []
    y_prec1 = []
    y_prec2 =[]
    y_prec3 = []
    y_prec4 = []
    y_prec5 = []
    y_prec6 = []
    y_prec7 =[]
    y_prec8 =[]
    for i in  range(len(hist['D_loss'])):        
        #print(hist['accuracy_classes'][i][0])
        y_accu0.append(hist['accuracy_classes'][i][0])
        y_accu1.append(hist['accuracy_classes'][i][1])
        y_accu2.append(hist['accuracy_classes'][i][2])
        y_accu3.append(hist['accuracy_classes'][i][3])
        y_accu4.append(hist['accuracy_classes'][i][4])
        y_accu5.append(hist['accuracy_classes'][i][5])
        y_accu6.append(hist['accuracy_classes'][i][6])
        y_accu7.append(hist['accuracy_classes'][i][7])
        y_accu8.append(hist['accuracy_classes'][i][8])
        
        y_rec0.append(hist['recall_classes'][i][0])
        y_rec1.append(hist['recall_classes'][i][1])
        y_rec2.append(hist['recall_classes'][i][2])
        y_rec3.append(hist['recall_classes'][i][3])
        y_rec4.append(hist['recall_classes'][i][4])
        y_rec5.append(hist['recall_classes'][i][5])
        y_rec6.append(hist['recall_classes'][i][6])
        y_rec7.append(hist['recall_classes'][i][7])
        y_rec8.append(hist['recall_classes'][i][8])
        
        y_prec0.append(hist['precision_classes'][i][0])
        y_prec1.append(hist['precision_classes'][i][1])
        y_prec2.append(hist['precision_classes'][i][2])
        y_prec3.append(hist['precision_classes'][i][3])
        y_prec4.append(hist['precision_classes'][i][4])
        y_prec5.append(hist['precision_classes'][i][5])
        y_prec6.append(hist['precision_classes'][i][6])
        y_prec7.append(hist['precision_classes'][i][7])
        y_prec8.append(hist['precision_classes'][i][8])
        
        y_pres0.append(hist['FPR_classes'][i][0])
        y_pres1.append(hist['FPR_classes'][i][1])
        y_pres2.append(hist['FPR_classes'][i][2])
        y_pres3.append(hist['FPR_classes'][i][3])
        y_pres4.append(hist['FPR_classes'][i][4])
        y_pres5.append(hist['FPR_classes'][i][5])
        y_pres6.append(hist['FPR_classes'][i][6])
        y_pres7.append(hist['FPR_classes'][i][7])
        y_pres8.append(hist['FPR_classes'][i][8])

    
    plt.plot(x, y_accu0, label=' Action')
    plt.plot(x, y_accu1, label=' Adventure ')
    plt.plot(x, y_accu2, label=' Comedy ')
    plt.plot(x, y_accu3, label=' Crime ')
    plt.plot(x, y_accu4, label=' Drama ')
    plt.plot(x, y_accu5, label=' Horror ')
    plt.plot(x, y_accu6, label=' Romance')
    plt.plot(x, y_accu7, label=' SciFi ')
    plt.plot(x, y_accu8, label=' Thriller ')    
    
    plt.xlabel('Iteration')
    plt.ylabel('Accuracy')
    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()
    #path = os.path.join(path, model_name + '_accuracy.png')
    plt.savefig(path+'accuracy_classes.png')
    plt.close()
    
    
    plt.plot(x, y_rec0, label=' Action')
    plt.plot(x, y_rec1, label=' Adventure ')
    plt.plot(x, y_rec2, label=' Comedy ')
    plt.plot(x, y_rec3, label=' Crime ')
    plt.plot(x, y_rec4, label=' Drama ')
    plt.plot(x, y_rec5, label=' Horror ')
    plt.plot(x, y_rec6, label=' Romance')
    plt.plot(x, y_rec7, label=' SciFi ')
    plt.plot(x, y_rec8, label=' Thriller ')    
    
    plt.xlabel('Iteration')
    plt.ylabel('Recall')
    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()
    #path = os.path.join(path, model_name + '_accuracy.png')
    plt.savefig(path+'Recall.png')
    plt.close()
    
    plt.plot(x, y_prec0, label=' Action')
    plt.plot(x, y_prec1, label=' Adventure ')
    plt.plot(x, y_prec2, label=' Comedy ')
    plt.plot(x, y_prec3, label=' Crime ')
    plt.plot(x, y_prec4, label=' Drama ')
    plt.plot(x, y_prec5, label=' Horror ')
    plt.plot(x, y_prec6, label=' Romance')
    plt.plot(x, y_prec7, label=' SciFi ')
    plt.plot(x, y_prec8 , label=' Thriller ')    
    
    plt.xlabel('Iteration')
    plt.ylabel('Precision')
    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()
    #path = os.path.join(path, model_name + '_accuracy.png')
    plt.savefig(path+'precision_classes.png')
    plt.close()
    
        
    plt.plot(x, y_pres0, label=' Action')
    plt.plot(x, y_pres1, label=' Adventure ')
    plt.plot(x, y_pres2, label=' Comedy ')
    plt.plot(x, y_pres3, label=' Crime ')
    plt.plot(x, y_pres4, label=' Drama ')
    plt.plot(x, y_pres5, label=' Horror ')
    plt.plot(x, y_pres6, label=' Romance')
    plt.plot(x, y_pres7, label=' SciFi ')
    plt.plot(x, y_pres8, label=' Thriller ')    
    
    plt.xlabel('Iteration')
    plt.ylabel('FPR')
    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()
    #path = os.path.join(path, model_name + '_accuracy.png')
    plt.savefig(path+'FPR_classes.png')
    plt.close()
    


def initialize_weights(net):
    for m in net.modules():
        if isinstance(m, nn.Conv2d):
            m.weight.data.normal_(0, 0.02)
            m.bias.data.zero_()
        elif isinstance(m, nn.ConvTranspose2d):
            m.weight.data.normal_(0, 0.02)
            m.bias.data.zero_()
        elif isinstance(m, nn.Linear):
            m.weight.data.normal_(0, 0.02)
            m.bias.data.zero_()